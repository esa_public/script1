// document.querySelector('#header10').innerHTML = "Modification from remote script.";


// Change color view div background color
var color_organizer = document.querySelectorAll('.color_organizer');
for ( var i = 0; i < color_organizer.length; i++){
    console.log(color_organizer[i].querySelector('.color_value'));
    color_organizer[i].querySelector('.color_view_div').style.backgroundColor = color_organizer[i].querySelector('.color_value').innerHTML;
}

// Add function to theme_div
var theme_div = document.querySelectorAll(".theme_div")
for(var theme_index = 0; theme_index < theme_div.length; theme_index++){
    console.log(`Add function to div ${theme_index}`);
    theme_div[theme_index].addEventListener("click", function(){
        var colors_spec = this.querySelectorAll(".font-color-model")
        console.log(`Nombre de couleurs trouvées = ${colors_spec.length}`)
        for (var c_spec = 0; c_spec < colors_spec.length; c_spec++){
            console.log(colors_spec[c_spec])
            var background_color = colors_spec[c_spec].style.backgroundColor;
            var fore_color = colors_spec[c_spec].style.color;
            var border_color = colors_spec[c_spec].style.borderColor;
            // Some notes
            console.log(`New background_color = ${background_color}`);
            console.log(`New fore_color = ${fore_color}`);
            console.log(`New border_color = ${border_color}`);
            var target_class = colors_spec[c_spec].querySelector(".font-color-class-name").innerHTML;
            console.log(`Target class = ${target_class}`);
            // var target_element = document.querySelectorAll(target_class);
            var target_element = document.getElementsByClassName(target_class);
            console.log(`Nb object to modify ${target_element.length}`)
            for(var target = 0; target < target_element.length; target++){
                target_element[target].style.backgroundColor = background_color;
                target_element[target].style.color = fore_color;
                target_element[target].style.borderColor = border_color;
            }
            // for(var target = 0; target < target_element.length; target++){
            //     if (target_class.includes("bg_") || target_class.includes("body_")){
            //         target_element[target].style.backgroundColor = new_color;
            //     }
            //     if (target_class.includes("text_")){
            //         target_element[target].style.color = new_color;
            //     }
            //     if (target_class.includes("bd_")){
            //         target_element[target].style.borderColor  = new_color;
            //     }
            // }
        }
    }, false);
}

// Hide element with hide_on_load class
var hide_elements = document.querySelectorAll(".hide_on_load");
for (var i = 0; i < hide_elements.length; i++){
    hide_elements[i].style.display = "none";
}